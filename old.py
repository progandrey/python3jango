test = {
    125 : 'test'
}
if 125 in test:# 125 not in test:
    print('Yes')

contacts = {
    'Ivan Ivanov' : '+38096'
}

testing = input('Кого ищем? :')

if testing in contacts:
    print('Контакт найден:' + contacts[testing])
else:
    print('Контакт не найден!')

print(contacts.get('Ivan a Petrov', 'Номер не найден')) # если не найдет, вернет текст
print(contacts['Ivan a Petrov']) # если не найдет, выдаст ошибку
''' Многострочный
коментарий '''
# pass - когда этот блок ничего не делает

digits2 = range(2, 101)[::2]
for i in digits2:
    print(i)#=> 2, 4, 6
digits3 = [1, 2, 3, 4, 5]
digits4 = digits3[::-1] # выполнит реверс списка
print(digits4) # 5, 4, 3, 2, 1

#####################33
# Форматирование строк
name = 'Джеси'
age = 21

print('Привет, %s!\n Тебе уже %d год!' % (name, age))
# %s - плейсхолдер строки
# %d - плейсхолдер числа
# %f - плейсхолдер дроби

print('Привет, {0}!\nТебе уже {} год!'.format(name, age))
print('Привет, {person_name}!\nТебе уже {person_age} год!'.format(person_name = name, person_age = age))

human = {
    'name': 'Jessy',
    'age': 21
}
print('Привет, {person[name]}!\nТебе уже {person[age]} год!'.format(person = human))
print('{0:*^10}'.format('text'))# какой символ, какой тип заполнинея и склько символов ширина строки
# Text*** <
# ****Text >
# **Text** ^

#################
#20 - Функции для работы со строками и числами
fruits = ['Limon', 'Apple', 'Kivi']
members = 'James,Jonny,Jessie'

# join, replace, startswith, endswith, lower, upper, split, min, max, abs, sum


