############################
##16 – Модули в языке Питон. Создание и работа с модулями
print(" ", end="\n\n")
print("#16 – Модули в языке Питон. Создание и работа с модулями")

from curses import wrapper
import time, datetime as d, sys, os, platform
from math import sqrt as s, ceil
from tokenize import Name
from xmlrpc.client import Boolean # импортируем только одну функцию корень

time.sleep(1) # заморозить программу на указаное знач. секунд
print(d.datetime.now().time().hour) # дата и время
print(sys.path) # какой путь к текущему файлу
print(os.name) # название системы
print(platform.system()) # название системы
print(ceil(s(25)))

# import mymodule as my
# print(my.name)
# my.hello()

# my module
from mymodule import add_three_numbers as add
print(add(5, 3, 1))

# PIP module (framework https://pypi.org/search/?q=django)
# import cowsay
# cowsay.cow('Hello World')


############################
#17 – Основы ООП. Создание класса и объекта
    # переменная = поле (в классе)  # функция = метод (в классе)
print(" ", end="\n\n")
print("#17 – Основы ООП. Создание класса и объекта")
class Cat:
    name = None
    age = None
    isHappy = None

    def set_data(self, name, age, isHappy):
        self.name = name # self - поле класса, через которое получаем доступ к полям класса
        self.age = age
        self.isHappy = isHappy

    def get_data(self):
        print(self.name, 'age:', self.age, 'Happy:', self.isHappy)

cat1 = Cat()
cat1.name = "Barsik"
cat1.age = 3
cat1.isHappy = True

cat2 = Cat()
cat2.set_data("Jopen", 2, False)
# print(cat1.name, cat2.name)
cat1.get_data()
cat2.get_data()


############################
#18 – Конструкторы, переопределение методов
print(" ", end="\n\n")
print("#18 – Конструкторы, переопределение методов")
class Cat2:
    name = None
    age = None
    isHappy = None

    def __init__(self, name = None, age = None, isHappy = None):
        self.set_data(name, age, isHappy)
        self.get_data()

    def set_data(self, name = None, age = None, isHappy = None):
        self.name = name # self - поле класса, через которое получаем доступ к полям класса
        self.age = age
        self.isHappy = isHappy

    def get_data(self):
        print(self.name, 'age:', self.age, 'Happy:', self.isHappy)

cat1 = Cat2("Barsik", 3, True)
cat2 = Cat2("Jopen", 2, False)
cat1.set_data('John', 1)
cat1.get_data()


############################
#19 – Наследование, инкапсуляция, полиморфизм
print(" ", end="\n\n")
print("#19 – Наследование, инкапсуляция, полиморфизм")
# Нету множественного наследования, наследование от одного класса
# Полиморфизм - функционал переписываем в классах наследниках
# Инкапсуляция __year защищает только от прямого вывода, изменять можно
class Building:
    __year = None
    city = None

    def __init__(self, year, city):
        self.year = year
        self.city = city

    def get_info(self):
        print("Year", self.year, ". City:", self.city)

class School(Building):
    pupils = 0

    def __init__(self, year, city, pupils):
        super().__init__(year, city)
        self.pupils = pupils

    def get_info(self):
        super().get_info()
        print('Pupils:', self.pupils)

class House(School):
    pass

class Shop(Building):
    pass

school = School(2000, "Kiev", 100)
school.get_info()
house = House(1200, 'Bachmach', 213)
house.get_info()
shop = Shop(1500, 'Bachmach11')
shop.get_info()


############################
#20 – Декораторы функций
# @validator подключение декоратора
print(" ", end="\n\n")
print("#20 – Декораторы функций")
import webbrowser

def validator(func):
    def wrapper(url):
        if "." in url:
            func(url)
        else:
            print('Неверный URL')
    return wrapper


@validator
def open_url(url):
    webbrowser.open(url)

open_url('http://azaza.com.ua')
