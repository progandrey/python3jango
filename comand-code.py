"""
django-admin startproject learning - устанавливаем django

python manage.py runserver - запуск сервера
Ctrl + C - остановка сервера

python manage.py startapp <name>- создаем новое приложение в нутри нашего проекта

##### Урок #4 - Шаблонизатор Jinja и HTML шаблоны
! Tab и появляется шаблон страницы

#7 - Работа с базой данных. Создание модели
python manage.py makemigrations
python manage.py migrate

python manage.py createsuperuser

http://127.0.0.1:8000/admin

"""



# python manage.py help - команды менеджера

# startproject - создание проекта

# startapp - создание нового приложения

# makemigrations - изм. миграции
# migrate - накатывание миграции

# runserver - запуск приложения (розвернуть)




# Декооратор
# @property
# @lsvl.setter



# from datetime import datetime as dt
# # from time import sleep

# class Player:

#     __LVL, __HEALTH = 1, 100
#     __slots__ = ['__lvl', '__health', '__born']

#     def __init__(self):
#         self.__lvl = Player.__LVL
#         self.__health = Player.__HEALTH
#         self.__born = dt.now()

#     @property
#     def lvl(self):
#         return self.__lvl, f'{dt.now() - self.__born}'

#     @lvl.setter
#     def lvl(self, numeric):
#         self.__lvl += Player.__typeTest(numeric)
#         if self.__lvl >= 100: self.__lvl = 100

#     @classmethod
#     def set_cls_field(cls, lvl=1, health=100):
#         cls.__LVL = Player.__typeTest(lvl)
#         cls.__HEALTH = Player.__typeTest(health)

#     @staticmethod
#     def __typeTest(value):
#         if isinstance(value, int):
#             return value
#         else:
#             raise TypeError ('Must be int')

# Замена строки 'Hello apple'.replace('apple', 'lime')
# >> Hello lime

# если слово начинается с указанного символа
# if (name.startswith('A')):
# 	print('True')

# endswith - то же, что и делает предыдущий метод, только проверяет соответствие последнего
# символа

# Присвоение сразу трех переменных
# a,b,c = map(int,input().split())

# // - деление натсела 41//12=3
# % - остаток 41%12 = 5
# 3*12+5=41

# отримання другого символа з 5-ти значного числа
# 47865 // 1000 % 10 = 7

# розложение чисел
# x=47865
# a=x//10000
# b=x//1000%10
# c=x//100%10
# d=x%10
# print(a,b,c,d,e) -> 4 7 8 6 5


# import math
# math.trunc(32.3233) -> 32 - отсикает дробную часть, аналог int(32.3233) ->32

# math.floor(32.99) -> 32 - округление в низ
# math.ceil(32.001) -> 33 округление в верх

# ascii code table
# ord('r') -> 114 - код символа

# text = 'afadfadfa'
# text.replace('a', '???') - меняем букву на знаки вопроса

# '12 23'.isdigit() - впроверяем числинная ли строка
# 'sdfsdfFF'.isapha() - проверяем символьная ли строока

# ' sdfsdf  '.strip() - удаляем пустые пробелы в начале и конце строки, есть так же rstrip() and lstrip()

# value = 5.2
# text = f"""Text {value*2}"""
# print(text)

# ####################
# gender = {
# 	'm': 'Дорогой',
# 	'f': 'Дорогая'
# }

# a = [
# 	['Семен', 'Семенович', 32.43, 'm'],
# 	['Тамара', 'Ивановна', 32.43, 'f'],
# 	['Михаил', 'Анатолийович', 32.43, 'm'],
# ]

# for name,midname,balance,g in a:
# 	text = f"""{gender[g]} {name} {midname}, баланс вашего лицевого счета
# составляет {balance} грн."""
# 	print(text)

# type(a) - Посмотреть тип

# sorted([43,2,64,3,-5,32],reverse=True) - сортировка перевернутая

# При обычном копировании в переменную копируется ссылка на обект, что бы создать новую переменную со своим обектом, делаем так
# a=[1,2,3]
# d=a[:] - перебираем весь обект срезами
# или d=a.copy() - копирование, создание отдельного обекта для d

# b.sort(reverse=True) - сортировка по убыванию


# for rindex, mumber in enumerate({1,2,3}): - получает и индекс
# 	print(number, index)

# ##### Вывод в обратном порядке
# x=int(input())
# while x>0:
# 	print(x%10)
	
# 	x=x//10 #10 - указывается система десятиричная, восмиричная и т.д.
# 7945 -> 5497




# selary - возможность делать запросы асинхронно

# python manager.py collectstatic

# cd tickets/frontend
# npm run build

