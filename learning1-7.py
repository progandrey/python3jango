# 3. Базовые операции
from operator import truediv


print("# 3. Базовые операции")
print(min(2, 4, -1, 7), max(2, 4, -1, 7))
print(abs(-5), pow(5, 3), round(5 / 3))
# // - диление приводим до целого, ** (pow) - указываем степень квадрата
print(5 / 2, 5 // 2)
print(5 ** 3, "Result: ", 7, 23, sep="|", end="!\n>")
print('Start \' \t \\ line \n.')


#4 – Переменные и типы данных
print(" ", end="\n\n")
print("#4 – Переменные и типы данных")

number = 5.23
name = "Номер: "
boolean = False
str_num = '4'
print(name + str(number + int(str_num)), boolean)

# удаление переменной
del number

num1 = int(input("Введите первое число: "))
num2 = int(input("Введите второе число: "))
num1 += 2
# -= *= /= %= &/

print(name * 3)
print('result:', num1 + num2)
print('result:', num1 - num2)
print('result:', num1 / num2)
print('result:', num1 * num2)
print('result:', num1 ** num2)
print('result:', num1 // num2)


#5 – Условные операторы <= == !=
print(" ", end="\n\n")
print("#5 – Условные операторы")

if not boolean:
    print('Boolean = true')
    if num1 > num2 and num1 != num2:
        print('num1 > num2')
    elif num1 == num2:
        print('num1 = num2')
    else:
        print('num1 < num2')

# if num1 == 2:
#     num2 = 1
# else:
#     num2 = 2

num2 = 1 if num1 == 2 else 2


#6 – Циклы и операторы в них (for, while)
print(" ", end="\n\n")
print("#6 – Циклы и операторы в них (for, while)")

for i in range(1, 6, 2):
    print(i, end="")

count = 0
for i in name:
    print(i * 3, end='')
    count += 1
    if i == ':':
        print('Yes')

print(count)

i = 5
while i < 15:
    print(i)
    i += 2

# inputExit = True
# while inputExit:
#     if input('exit') == 'exit':
#         inputExit = False

for i in range(1, 15):
    if i >= 7:
        break

    if i % 2 == 0:
        continue

    print(i)

found = None
for i in 'hello':
    if i == 'l':
        found = True
        break
    else:
        found = False

print(found)

#7 – Списки (list). Функции и их методы
print(" ", end="\n\n")
print("#7 – Списки (list). Функции и их методы")

nums = [3, 4, True, 8, True, 'text', 5.23, [2, 3]]
nums[0] = 1
print(nums)
print(nums[-1][1])

nums.append(100)
nums.insert(0, True)
nums.extend([12, 14, 15])
print(nums)
# nums.sort()
# nums.reverse()
# удаляет посл. елемент в списке
nums.pop()
# удаляет елемент в списке по индексу 2
nums.pop(2)
# удаляет елемент в списке по индексу второй с конца
nums.pop(-2)
# удаление по значению
nums.remove(True)

# удаляем все
# nums.clear()

print(nums)
print(nums.count(True))
print(len(nums))

for el in nums:
    el *= 2
    print(el)


n = int(input('Enter length: '))
user_list = []
i = 0
while i < n:
    user_list.append(input('Enter rlrmrnt #' + str(i + 1) + ': '))
    i += 1

print(user_list)