#8 – Функции строк. Индексы и срезы
print(" ", end="\n\n")
print("#8 – Функции строк. Индексы и срезы")

word = 'Hello world and not world'

print(len(word))
print(word[3])
print(word.count('l'))
print(word.upper())
print(word.isupper())

print(word.lower())
print(word.islower())

print(word.capitalize())

# ищет индекс искомого символа
print(word.find('ll'))

# приводит строку в список
string = word.split(' ')
print(string)
print(string[2])

for i in range(len(string)):
    string[i] = string[i].capitalize()

result = ', '.join(string)
print(result)


# срезы вырезаем с какого по какой символ с шагом
print(result[0:9])
print(result[8:])
print(result[9:-3])
print(result[9:-3:2])

list = [1, 2, 3, True, 4.5, 6, 7]
print(list[2:-1:2])
print(list[::-2])


############################
#9 – Кортежи (tuple) - это как константа, которую 
# не можно переопредилять и весят меньше чем списки
print(" ", end="\n\n")
print("#9 – Кортежи (tuple)")

data = (3, 2, 5, 7, True, 4.3, 'Hello')
# data = 3, 2, 5, 7, True, 4.3, 'Hello' - картеж можна задавать без скобок
#data[1] = 3 - Не можна, ошибка
print(data[3:5])
print(data.count(5))
print(len(data))

for el in data:
    print(el)

# преобразование в картеж
nums = [5, 7, 8]
new_data = tuple(nums)
print(new_data)

word = tuple('Hello World')
print(word)


############################
#10 – Словари (dict) и работа с ними
print(" ", end="\n\n")
print("#10 – Словари (dict) и работа с ними")

# [] - списки, () - картеджи, {} - словари (в представлении ключ:значение {3:5, 7:1})
# {} - множества (в представлении данних {4, 5, 2})

test = {3:5, True:8, (5, 6):9, 'key':'value'}
print(test[3])
print(test[True])
print(test[(5, 6)])
print(test['key'])

country = {'code':'UK', 'name':'Ukranian', 'population':44}
# country = dict(code='UK', name='Ukranian', population=44)

# получение только ключей или значений
print(country.keys())
print(country.values())

print(country['name'])
print(country.get('name'))
print(country.items())

for key, value in country.items():
    print(key, ' - ', value)



country['name'] = 'Ukkkkr'
print(country)

# удаляет ключ и значение по ключу
country.pop('name')
print(country)

# удаляет последний елемент
country.popitem()
print(country)


# чистить словарь
country.clear()
print(country)


person = {
    'user_1': {
        'name':'Andrey',
        'age':34,
        'address': ['g. Kiev', 'st. Peremogi', 23],
        'grades':{'math':4, 'algebra':5}
    },
    'user_2': {
        'name':'Vasya',
        'age':60,
        'address': ('g. Kiev1', 'st. Peremogi1', 25)
    }
}

print(person['user_1']['address'][1])
print(person['user_1'])

