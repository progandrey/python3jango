from django.shortcuts import render, redirect
from .models import News
from .forms import NewsForm
from django.views.generic import DetailView, UpdateView, DeleteView


class NewsDetailView(DetailView):
    model = News
    template_name = 'news/details_view.html'
    context_object_name = 'news'


class NewsUpdateView(UpdateView):
    model = News
    template_name = 'news/create.html'

    form_class = NewsForm


class NewsDeleteView(DeleteView):
    model = News
    success_url = '/news/'
    template_name = 'news/news-delete.html'


def news_home(request):
    news = News.objects.order_by('-date')
    #.all() [1:3]"-" - выводит в обр. порядке
    return render(request, 'news/news_home.html', {'title': 'Новости', 'news': news})


def news_create(request):
    error = ''
    if request.method == 'POST':
        form = NewsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('news_home')
        else:
            error = 'Форма была не верной'

    form = NewsForm()
    data = {
        'title': 'Создание новости',
        'error': error,
        'form': form
    }

    return render(request, 'news/create.html', data)