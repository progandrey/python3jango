# Калькулятор

class Calculator:
    operator = None
    inp1 = None
    inp2 = None

    def __init__(self):
        self.operator = input('Введите оператор (+-*/): ')
        self.inp1 = float(input('Введите первое значение: '))
        self.inp2 = float(input('Введите второе значение: '))

        print(self.inp1, self.operator, self.inp2, '=', end='')

        if self.operator == '+':
            print(self.plus())
        elif self.operator == '-':
            print(self.minus())
        elif self.operator == '*':
            print(self.umnogenie())
        elif self.operator == '/':
            print(self.delenie())

    def plus(self):
        return self.inp1 + self.inp2

    def minus(self):
        return self.inp1 - self.inp2

    def umnogenie(self):
        return self.inp1 * self.inp2

    def delenie(self):
        return self.inp1 / self.inp2


calc = Calculator()
