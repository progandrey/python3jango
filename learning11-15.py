############################
#11 – Множества (set и frozenset) 
# елементы в случайном порядке и не может быть дублирующих значений
print(" ", end="\n\n")
print("#11 – Множества (set и frozenset)")

data = set('hello')
print(data)

data = {3, 7, 2, 7, 9}

print(data)

# в множестве не можем обратиться к елементу по индексу или поменять
# добавляем или обновляем
data.add(32)
data.update(['32', True, 4, 6])
print(data)

# удаляем
data.remove(True)
print(data)

# удаляет первый елемент data.clear() - очищает множество
data.pop()

nums = [5, 3, 7, 5, 5]
nums = set(nums)
print(nums)

# frozenset - замороженное множество, не можно менять елементы (как в кортеджах)

new_data = frozenset([3, 7, 2, 7, 9, '32', True, 4, 6, 5, 3, 7, 5, 5])
print(new_data)


############################
#12 – Функции (def, lambda)
# вынос повторяющего кода в одно место
print(" ", end="\n\n")
print("#12 – Функции (def, lambda)")

def test_func(text):
    # pass
    print(text, end='')
    print('!')

# pass - не виполнится ничего
test_func('Hi')
test_func(4)
test_func(8.4)

def summa(a, b):
    return a + b

rez = summa(3, 6)
print(rez)
print(summa('H', 'i'))

def minimal(l):
    min_number = l[0]
    for el in l:
        if el < min_number:
            min_number = el
    
    return min_number

nums1 = [3.1, 2.1, 5.2, 2.8, 9.1]
print(minimal(nums1))

# анонимные фун. возвращает значение без return
func = lambda x, y: x * y
print(func(5, 2))

############################
#13 – Работа с файлами за счет Питон
print(" ", end="\n\n")
print("#13 – Работа с файлами за счет Питон")
# file = open('logs/text.txt', 'w')

# file.write('Hello my frend\n')
# file.write('!!!')
# file.close()

# r - открытие на чтения (является значение по умолчанию)
# w - открытие на запись, содержание удаляется, если файл не существует, создается новый
# x - открытие на запись, если файла не существует, иначе исключение
# a - append к текущей информации добавляем новую информацию
# b - открытие в двоичном режиме
# t - открытие в текстовом режиме (является значение по умолчанию)
# + - открытие на чтение и запись

data = input('Input text: ')
file = open('logs/text.txt', 'a')
file.write(data + '\n')
file.close()

file = open('logs/text.txt', 'r')
# выводит текст из файла, можно указываем кол. выводимих символов
# print(file.read(12))

# считываем по строчно
for line in file:
    print(line, end='')
file.close()

############################
#14 – Обработчик исключений. Конструкция «try - except»
# исключения - ошибки, которые возникают в ходе виполнения программи
print(" ", end="\n\n")
print("#14 – Обработчик исключений. Конструкция «try - except»")

x = 0
while x ==0:
    try:
        x = int(input('Input number: '))
        # x +=5
        x = 5/x
        print(x)
    except ZeroDivisionError:
        print('Input not null!')
    except ValueError:
        print('Input number!')
    else:
        print('Else')
    finally:
        print('End')

# удобро при работе с файлами, в tru открываем файл а в finally закрываем


############################
#15 – Менеджер «With ... as» для работы с файлами
print(" ", end="\n\n")
print("#15 – Менеджер «With ... as» для работы с файлами")

try:
    file = open('text.txt', 'r')
    print(file.read())
except FileNotFoundError:
    print('Файл не найден!')
finally:
    file.close()


try:
    with open('text.txt', 'r', encoding='utf-8') as file:
        print(file.read())
except FileNotFoundError:
    print('Файл не найден!')
